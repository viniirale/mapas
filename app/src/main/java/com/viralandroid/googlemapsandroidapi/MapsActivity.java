package com.viralandroid.googlemapsandroidapi;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import android.location.LocationListener;

import org.w3c.dom.Document;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        // magia pra poder fazer o request HTTP em GMapV2Direction.getDocument na thread principal, mudar depois
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void showToast(CharSequence message){
        Context context = getApplicationContext();
        CharSequence text = message;
        int duration = Toast.LENGTH_LONG;

        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

//CalculaRotas(origem, destino, tamanho da linha da rota, cor da linha, endereço(sempre deixar Address))
    public void CalculaRotas(LatLng source, LatLng dest, int tam_linha, int cor, Address address)
    {
        GMapV2Direction md = new GMapV2Direction();
        Document doc = md.getDocument(source, dest, GMapV2Direction.MODE_DRIVING);
        if (doc == null) {
            this.showToast("Falha ao buscar rota para o endereço \"" + address.getAddressLine(0) + "\"");
            return;
        }
        ArrayList<LatLng> route = md.getDirection(doc);
        PolylineOptions rectLine =
                new PolylineOptions()
                        .width(tam_linha)
                        .color(cor);

        for (int i = 0; i < route.size(); i++)
            rectLine.add(route.get(i));

        Polyline polylin = mMap.addPolyline(rectLine);
    }

    public void BotaoPesquisa(View view) {
        EditText locationSearch = (EditText) findViewById(R.id.editText);
        String location = locationSearch.getText().toString();
        List<Address> addressList = null;

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        if (location != null || !location.equals(""))
        {
            Geocoder geocoder = new Geocoder(this);
            try
            {
                addressList = geocoder.getFromLocationName(location, 1);

            } catch (IOException e)
            {
                e.printStackTrace();
            }
            mMap.clear();

            //caso não encontre um endereço válido
            if (addressList.isEmpty()) {
                this.showToast("Não foi possível localizar o endereço \"" + location + "\"");
                return;
            }
            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.addMarker(new MarkerOptions().position(latLng).title(address.getFeatureName()).snippet(address.getAddressLine(2)));
            mMap.addMarker(new MarkerOptions().position(new LatLng(-30.056746, -51.174595)).title("Carona").snippet("PUCRS"));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-30.057751, -51.1757271), 12));

            // Gets the route
            LatLng sourcePos = new LatLng(-30.068879, -51.120660);
            LatLng destPos = new LatLng(address.getLatitude(), address.getLongitude());
            LatLng pucPos = new LatLng(-30.056746, -51.174595);




            CalculaRotas(sourcePos, destPos, 9, Color.RED, address);
            CalculaRotas(sourcePos, pucPos, 5, Color.BLUE, address);
            CalculaRotas(pucPos, destPos, 5, Color.BLUE, address);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;
        //move a camera para o ponto inicial (no caso o inf como exemplo)
        LatLng pos_inicial = new LatLng(-30.068879, -51.120660);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                pos_inicial, 17)
        );
        //verifica se o celular permite o uso da localizacao, caso for positivo, ativa o botao do MAPS pra localizar o usuario
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);


    }
}